package com.Calculadora.Calculadora.Service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatematicaService {

    public int adicao(List<Integer> numeros) {
        int resultado = numeros.get(0);
        for (int i=1; i<numeros.size(); i++) {
            resultado +=numeros.get(i);
        }
        return resultado;
    }

    public int subtracao(List<Integer> numeros) {
        int resultado = numeros.get(0);
        for (int i=1; i<numeros.size(); i++) {
            resultado -=numeros.get(i);
        }
        return resultado;
    }

    public int multiplicacao(List<Integer> numeros) {
        int resultado = numeros.get(0);
        for (int i=1; i<numeros.size(); i++) {
            resultado *=numeros.get(i);
        }
        return resultado;
    }

    public int divisao(int numUm, int numDois) {
        int resultado = numUm / numDois;
        return resultado;
    }
}
