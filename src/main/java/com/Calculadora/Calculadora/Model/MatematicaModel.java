package com.Calculadora.Calculadora.Model;

import java.util.List;

public class MatematicaModel {


    private List<Integer> numeros;

    public MatematicaModel() {
    }

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
}
