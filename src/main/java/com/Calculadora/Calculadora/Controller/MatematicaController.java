package com.Calculadora.Calculadora.Controller;

import com.Calculadora.Calculadora.Model.MatematicaModel;
import com.Calculadora.Calculadora.Service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PostMapping("adicao")
    public Integer adicao(@RequestBody MatematicaModel matematica) {
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || numeros.size() < 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números para serem somados");
        }
        return matematicaService.adicao(numeros);
    }

    @PostMapping("subtracao")
    public Integer subtracao(@RequestBody MatematicaModel matematica) {
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || numeros.size() < 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números para serem subtraidos");
        }
        return matematicaService.subtracao(numeros);
    }

    @PostMapping("multiplicacao")
    public Integer multiplicacao(@RequestBody MatematicaModel matematica) {
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || numeros.size() < 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números para serem multiplicados");
        }
        return matematicaService.multiplicacao(numeros);
    }

    @PostMapping("divisao")
    public int divisao(@RequestBody MatematicaModel matematica) {
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || numeros.size() < 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 números para serem multiplicados");
        }
        return matematicaService.divisao(10,2);
    }
}
